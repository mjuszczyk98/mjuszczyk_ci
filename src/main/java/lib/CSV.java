package lib;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSV {
	
	private String separator;
	private String path;
	
	public CSV(String path, String separator){
		this.path = path;
		this.separator = separator;
	}
	
	public List<String> readFields(String newSeparator, int...fields) {
		List<String> list = null;
		String line;
		String[] f;
		String resultLine;
		try (BufferedReader br = new BufferedReader(new FileReader(path))){
			list = new ArrayList<String>();
			line = br.readLine();
			while(line != null) {
				f = line.split(this.separator);
				resultLine = "";
				for(int i : fields) {
					if(resultLine.equals(""))
						resultLine = f[i];
					else
						resultLine += newSeparator + f[i]; 
				}
				list.add(resultLine);
				line = br.readLine();
			}
			br.close();
 			
		} catch (Exception e) {
			System.out.println("Wrong read path!!!");
		}
		return list;
	}
	public void toFile(String path, List<String> list) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(path));
			for(String s : list) {
				bw.write(s);
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			System.out.println("Wrong write path!!!");
		}
		
		
		
	}
	
	
}
